
// MainDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MatrixToBmp.h"
#include "MainDlg.h"
#include "afxdialogex.h"
#include "StrTools.h"
#include "BmpTools.h"

#include <iostream>
#include <fstream>
#include <string>
using namespace std;


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMainDlg 对话框



CMainDlg::CMainDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMainDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMainDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CMainDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMainDlg::OnBnClickedCancel)
	ON_BN_CLICKED(ID_BTN_FOUR_COLOR, &CMainDlg::OnBnClickedBtnFourColor)
END_MESSAGE_MAP()


// CMainDlg 消息处理程序

BOOL CMainDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMainDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMainDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMainDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMainDlg::OnBnClickedOk()
{
	//CDialogEx::OnOK();

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, _T("Describe Files (*.txt)|*.txt|All Files (*.*)|*.*||"), NULL);
	if (dlg.DoModal()) {

		CString fname = dlg.GetPathName();
		
		ifstream infile(fname, ios::binary | ios::in);
		infile.seekg(0, ios::end);
		int len = infile.tellg();
		BYTE* data = new BYTE[len + 2];
		infile.seekg(0, ios::beg);
		infile.read((char*)data, len);
		infile.close();
		data[len] = 0x0;
		data[len+1] = 0x0;

		CString content = AtoW(data, len-1);
		delete[] data;

		st_bw_image image;
		if (ParseContentMonoImage(content, image)) {

			if (SaveBWBmpFile(image, (fname + _T(".bmp")).GetBuffer(0))) {
				MessageBox(_T("转换成功！"));
			}
			else {
				MessageBox(_T("保存文件失败！请检查文件夹属性！"), _T("错误"), MB_ICONEXCLAMATION);
			}
		}
		else {
			MessageBox(_T("文件内容解析失败！"), _T("错误"), MB_ICONEXCLAMATION);
		}

		delete[] image.img;
	}
}

void CMainDlg::OnBnClickedBtnFourColor()
{
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, _T("Describe Files (*.txt)|*.txt|All Files (*.*)|*.*||"), NULL);
	if (dlg.DoModal()) {

		CString fname = dlg.GetPathName();
		
		ifstream infile(fname, ios::binary | ios::in);
		infile.seekg(0, ios::end);
		int len = infile.tellg();
		BYTE* data = new BYTE[len + 2];
		infile.seekg(0, ios::beg);
		infile.read((char*)data, len);
		infile.close();
		data[len] = 0x0;
		data[len+1] = 0x0;

		CString content = AtoW(data, len-1);
		delete[] data;

		st_four_color_image image;
		if (ParseContent4colorImage(content, image)) {

			if (Save_4ColorImage_To_4BitBmpFile(image, (fname + _T(".bmp")).GetBuffer(0))) {
				MessageBox(_T("转换成功！"));
			}
			else {
				MessageBox(_T("保存文件失败！请检查文件夹属性！"), _T("错误"), MB_ICONEXCLAMATION);
			}
		}
		else {
			MessageBox(_T("文件内容解析失败！"), _T("错误"), MB_ICONEXCLAMATION);
		}

		delete[] image.img;
	}
}


void CMainDlg::OnBnClickedCancel()
{
	// 退出程序
	CDialogEx::OnCancel();
}

bool CMainDlg::ParseContentMonoImage(CString content, st_bw_image& image)
{
	CStringArray lines;
	int size = Split(lines, TrimAll(content), _T("\n"));
	if (0 == size) {
		return false;
	}
	image.h = size;

	//确定图片的宽度
	int wbytes = 0;
	int i;
	for (i = 0; i < image.h; i++) {

		CStringArray bytes;
		int line_bytes = Split(bytes, TrimAll(lines[i]), _T(","));

		if (line_bytes > wbytes) {
			wbytes = line_bytes;
		}
	}
	image.w = wbytes * 8;

	//开辟内存空间
	image.img = new BYTE[wbytes * image.h];
	memset(image.img, 0xFF, wbytes * image.h); //默认白色

	//转换并写入
	for (i = 0; i < image.h; i++) {
		CStringArray bytes;
		int line_bytes = Split(bytes, TrimAll(lines[i]), _T(","));
		
		int j;
		for (j = 0; j < line_bytes; j++) {

			image.img[wbytes * i + j] = (BYTE)_tcstoul(bytes[j], NULL, 16);
		}
	}
	return true;
}


bool CMainDlg::ParseContent4colorImage(CString content, st_four_color_image& image)
{
	CStringArray lines;
	int size = Split(lines, TrimAll(content), _T("\n"));
	if (0 == size) {
		return false;
	}
	image.h = size;

	//确定图片的宽度
	int w = 0;
	int i;
	for (i = 0; i < image.h; i++) {

		CStringArray bytes;
		int line_bytes = Split(bytes, TrimAll(lines[i]), _T(","));

		if (line_bytes > w) {
			w = line_bytes;
		}
	}
	image.w = w;

	//开辟内存空间
	image.img = new char[w * image.h];
	memset(image.img, 0, w * image.h);

	//转换并写入
	for (i = 0; i < image.h; i++) {
		CStringArray bytes;
		int line_bytes = Split(bytes, TrimAll(lines[i]), _T(","));
		
		int j;
		for (j = 0; j < line_bytes; j++) {

			image.img[w * i + j] = (char)_ttoi(bytes[j]);
		}
	}
	return true;
}
