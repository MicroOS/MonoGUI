// Images.h: global images.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////
// 定义图像按钮使用的的图片

#if !defined(__IMAGES_H__)
#define __IMAGES_H__
//

class BWImgMgt;
void InitImageMgt (BWImgMgt* pImgMgt);

#define ID_IMG_BTN_001    200101
#define ID_IMG_BTN_002    200102
#define ID_IMG_BTN_003    200103

//
#endif // !defined(__IMAGES_H__)
