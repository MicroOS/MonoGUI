// DemoMain.cpp : Defines the entry point for the application.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////
#include <Windows.h>
#include <imm.h>
#pragma comment (lib, "imm32.lib") 

HIMC g_hImc;
void DisableIME(HWND hWnd)
{
    g_hImc = ImmGetContext(hWnd);
    if (g_hImc) {
        ImmAssociateContext(hWnd, NULL);
	}
    ImmReleaseContext(hWnd, g_hImc);
    ::SetFocus(hWnd);
}

#ifdef _DEBUG
#pragma comment (lib,"./Debug/MonoGUI.lib")
#else
#pragma comment (lib,"./Release/MonoGUI.lib")
#endif

#include "../MonoGUI/MonoGUI.h"
#include "DemoApp.h"
#include "Desktop.h"
#include "Images.h"
////

#if defined (MOUSE_SUPPORT)
// 定义箭头光标
static BYTE pcaCrusor_Arrow_ANDPlane[128] =
{
0xff,0xff,0xff,0xff,0xdf,0xff,0xff,0xff,0x87,0xff,0xff,0xff,0xc1,0xff,0xff,0xff,
0xc0,0x7f,0xff,0xff,0xe0,0x1f,0xff,0xff,0xe0,0x07,0xff,0xff,0xf0,0x01,0xff,0xff,
0xf0,0x00,0x7f,0xff,0xf8,0x00,0x1f,0xff,0xf8,0x00,0x07,0xff,0xfc,0x00,0x01,0xff,
0xfc,0x00,0x00,0x7f,0xfe,0x00,0x00,0x1f,0xfe,0x00,0x00,0x07,0xff,0x00,0x00,0x01,
0xff,0x00,0x00,0x01,0xff,0x80,0x00,0x1f,0xff,0x80,0x00,0xff,0xff,0xc0,0x03,0xff,
0xff,0xc0,0x07,0xff,0xff,0xe0,0x0f,0xff,0xff,0xe0,0x1f,0xff,0xff,0xf0,0x1f,0xff,
0xff,0xf0,0x3f,0xff,0xff,0xf8,0x3f,0xff,0xff,0xf8,0x3f,0xff,0xff,0xfc,0x7f,0xff,
0xff,0xfc,0x7f,0xff,0xff,0xfe,0x7f,0xff,0xff,0xfe,0x7f,0xff,0xff,0xff,0xff,0xff 
};

static BYTE pcaCrusor_Arrow_XORPlane[128] =
{
0xc0,0x00,0x00,0x00,0xc0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x08,0x00,0x00,0x00,0x06,0x00,0x00,0x00,0x07,0x80,0x00,0x00,0x03,0xe0,0x00,0x00,
0x03,0xf8,0x00,0x00,0x01,0xfe,0x00,0x00,0x01,0xff,0x80,0x00,0x00,0xff,0xe0,0x00,
0x00,0xff,0xf8,0x00,0x00,0x7f,0xfe,0x00,0x00,0x7f,0xff,0x80,0x00,0x3f,0xff,0xe0,
0x00,0x3f,0xff,0x00,0x00,0x1f,0xfc,0x00,0x00,0x1f,0xf8,0x00,0x00,0x0f,0xf0,0x00,
0x00,0x0f,0xe0,0x00,0x00,0x07,0xc0,0x00,0x00,0x07,0x80,0x00,0x00,0x03,0x80,0x00,
0x00,0x03,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 
};
#endif // defined(MOUSE_SUPPORT)

static DemoApp  g_App;
static Desktop  g_Desktop;
////

LRESULT CALLBACK WndProc (HWND, UINT, WPARAM, LPARAM);
////
int APIENTRY WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{
	MSG msg;

	WNDCLASSEX wcex;

	wcex.cbSize			= sizeof(WNDCLASSEX); 
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= NULL;

#if defined (MOUSE_SUPPORT)
	wcex.hCursor		= CreateCursor(
								hInstance,        // handle to application instance
								0,0,              // position of hot spot
								32,32,            // cursor width and height
								pcaCrusor_Arrow_ANDPlane,
								pcaCrusor_Arrow_XORPlane );
#else
	wcex.hCursor		= NULL;
#endif // defined(MOUSE_SUPPORT)

	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "MonoGUI_Win32_DEMO";
	wcex.hIconSm		= NULL;

	RegisterClassEx(&wcex);

	// Perform application initialization:
	HWND hWnd = CreateWindow("MonoGUI_Win32_DEMO", "MonoGUI模拟系统", WS_SYSMENU,
		200, 200, SCREEN_W*SCREEN_MODE+16, SCREEN_H*SCREEN_MODE+39, NULL, NULL, hInstance, NULL);

    // 重要 ！！！下列代码禁用中文输入法
	// 如果不禁用输入法，会影响本系统的按键映射关系
	DisableIME (hWnd);

	// MonoGUI主窗口初始化
	if (! g_App.Create(&g_Desktop, hWnd)) {

		char sErrInfo[200];
		sprintf (sErrInfo, "程序目录下缺少 %s 文件，导致键盘按键不能被正确映射！", KEY_MAP_FILE);
		MessageBox (hWnd, sErrInfo, "出错信息", MB_OK);
	}

	//初始化图片管理器，OImgButton控件会用到图片资源
	InitImageMgt (g_App.m_pImageMgt);

	// 创建主窗口
	g_Desktop.Create(NULL,WND_TYPE_DESKTOP,WND_STYLE_NORMAL,WND_STATUS_NORMAL,0,0,SCREEN_W,SCREEN_H,0);

	// 显示Win32主窗口
	ShowWindow(hWnd, nCmdShow);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		TranslateMessage(&msg);

		// 将系统消息转送给MonoGUI的消息队列
		g_App.InsertMsgToMonoGUI (&msg);
		// 转送消息结束

		// 分配给MonoGUI的运行时间片
		g_App.RunInWin32 ();
		
		DispatchMessage (&msg);

		::Sleep(10);
	}

	return msg.wParam;

/*
	// 模板方法生成对话框
	ODialog* pTestDlg = new ODialog ();
	pTestDlg->CreateFromTemplet (g_Desktop.m_pMainWnd, "dem\100.dtm");
*/
/*	// 测试组合框
	OCombo* pComboTest = new OCombo();
	pComboTest->Create( pTest, WND_TYPE_COMBO, WND_STYLE_NORMAL|WND_STYLE_DISABLE_IME, WND_STATUS_NORMAL, 30, 30, 135, 20, 5);
	pComboTest->SetText( "12345", 20 );
	pComboTest->AddString( "001_1_试验_试验_试验_试验" );
	pComboTest->AddString( "002_2_试验_试验_试验_试验" );
	pComboTest->AddString( "003_3_试验_试验_试验_试验" );
	pComboTest->AddString( "004_4_试验_试验_试验_试验" );
	pComboTest->AddString( "005_5_试验_试验_试验_试验" );
	pComboTest->AddString( "006_6_试验_试验_试验_试验" );
	pComboTest->AddString( "007_7_试验_试验_试验_试验" );
	pComboTest->AddString( "008_8_试验_试验_试验_试验" );
	pComboTest->AddString( "009_9_试验_试验_试验_试验" );
	pComboTest->SetDroppedLinage( 5 );
	pComboTest->EnableEdit( TRUE );
	pComboTest->EnableDropDown( TRUE );
*/
	// 测试列表框
/*	OList* pListTest = new OList ();
//	pListTest->Create( pTest, WND_TYPE_LIST,WND_STYLE_NORMAL,WND_STATUS_NORMAL, 20, 20, 135, 80, 5);
	pListTest->AddString( "001_1_试验_试验_试验_试验" );
	pListTest->AddString( "002_2_试验_试验_试验_试验" );
	pListTest->AddString( "003_3_试验_试验_试验_试验" );
	pListTest->AddString( "004_4_试验_试验_试验_试验" );
	pListTest->AddString( "005_5_试验_试验_试验_试验" );
	pListTest->AddString( "006_6_试验_试验_试验_试验" );
	pListTest->AddString( "007_7_试验_试验_试验_试验" );
	pListTest->AddString( "008_8_试验_试验_试验_试验" );
	pListTest->AddString( "009_9_试验_试验_试验_试验" );
	pListTest->AddString( "010_10_试验_试验_试验_试验" );
	pListTest->AddString( "011_11_试验_试验_试验_试验" );
	pListTest->AddString( "012_12_试验_试验_试验_试验" );
	pListTest->AddString( "013_13_试验_试验_试验_试验" );
	pListTest->AddString( "014_14_试验_试验_试验_试验" );
	pListTest->AddString( "015_15_试验_试验_试验_试验" );
	pListTest->AddString( "016_16_试验_试验_试验_试验" );
	pListTest->AddString( "017_17_试验_试验_试验_试验" );
	pListTest->AddString( "018_18_试验_试验_试验_试验" );
	pListTest->AddString( "019_19_试验_试验_试验_试验" );
	pListTest->AddString( "020_20_试验_试验_试验_试验" );
	pListTest->InsertString( 10, "TestInsert" );
	int iIndex = pListTest->FindString( "004_4_试验_试验_试验_试验" );
	pListTest->DeleteString( 3 );
	int f = 0;
	pListTest->Create( pTest, WND_TYPE_LIST,WND_STYLE_NORMAL,WND_STATUS_NORMAL, 20, 20, 135, 80, 5);
*/
/*	// 测试进度条
	OProgressBar* pTestProgress = new OProgressBar ();
	pTestProgress->Create (pTest, WND_TYPE_BUTTON, WND_STYLE_NORMAL, WND_STATUS_NORMAL, 70, 62, 70, 10, 2);
	pTestProgress->SetRange (100);
	pTestProgress->SetPos (20);
*/
/*	// 试验拼音数据库
	OIME_DB db;
	db.Init ();
	db.SetCurIME (2);

	db.SyAdd ('9');
	db.SyAdd ('2');
	db.SyAdd ('6');
	db.SyAdd ('4');
//	db.SyAdd ('6');
//	db.SyAdd ('4');
	// 试验数字查拼音的查询速度
	int a = clock();
	BYTE* pSyTable = NULL;
	int iCount = db.GetSy (&pSyTable);
	int b = clock() - a;
	int c = 0;
	// 试验查询速度结束

	// 试验拼音查汉字
	char cInput[] = "wang  ";
	BYTE* pHzTable = NULL;
	int iHzCount = db.GetHz ((BYTE*)cInput, &pHzTable);
	int d = 0;
	// 试验拼音查汉字结束

	// 试验汉字查联想
	char cCharacter[] = "王";
	BYTE* pLxTable = NULL;
	int iLxCount = db.GetLx ((BYTE*)cCharacter, &pLxTable);
	int e = 0;
	// 试验汉字查联想结束
*/
///////拼音库试验结束

	// 试验MessageBox
//	char cInformation[] = "123456789\nabcdefg\n中文字符中文字符中文字符";
	// 老方法
	/*
	OMsgBoxDialog* pMessageBox = new OMsgBoxDialog ();
	pMessageBox->Create (pTest, "Information", cInformation, MB_INFORMATION|MB_SOLID|MB_ROUND_EDGE);
	*/
	// 新方法
//	OMsgBox (pTest, "Information", cInformation, MB_ERROR|MB_SOLID|MB_ROUND_EDGE);

	// 测试IsChinese函数
/*
	BOOL b = IsChinese (cInformation, 16, TRUE);
	if (b)
	{
		OMsgBox (pTest, "", "是中文", TRUE);
	}
	else
	{
		OMsgBox (pTest, "", "不是中文", TRUE);
	}
*/

	// 测试IsPosValid函数
/*
	BOOL b = IsPosValid (cInformation, 29);
	if (b)
	{
		OMsgBox (pTest, "", "位置合法", 0);
	}
	else
	{
		OMsgBox (pTest, "", "位置不合法", 0);
	}
*/

/*
	// 老方法生成对话框
	// 子窗口套接试验
	ODialog* pTest = new ODialog ();
	pTest->Create(g_Desktop.m_pMainWnd,WND_TYPE_DESKTOP,WND_STYLE_NORMAL,WND_STATUS_NORMAL,30,10,100,60,0);
	pTest->SetText ("窗口002",20);
	pTest->m_wWndType = WND_TYPE_DIALOG;
	g_Desktop.m_pMainWnd->m_pActive = pTest;

	ODialog* pTest2 = new ODialog ();
	pTest2->Create(pTest,WND_TYPE_DESKTOP,WND_STYLE_NORMAL,WND_STATUS_NORMAL,50,30,130,80,0);
	pTest2->SetText("窗口001",20);
	pTest2->m_wWndType = WND_TYPE_DIALOG;

	pTest->m_pActive = pTest2;
	// 子窗口套接试验结束

	// 对话框样式的实验
	pTest->m_wStyle |= WND_STYLE_ROUND_EDGE;
	pTest->m_wStyle |= WND_STYLE_SOLID;
	pTest->m_wStyle |= WND_STYLE_NO_TITLE;
	pTest2->m_wStyle |= WND_STYLE_ROUND_EDGE;
	pTest2->m_wStyle |= WND_STYLE_SOLID;
	pTest2->m_wStyle |= WND_STYLE_NO_TITLE;
	// 对话框样式试验结束

	// WND_STYLE_NO_BORDER
	// 试验各种控件
	// OStatic控件
	OStatic* pTest3 = new OStatic ();
	pTest3->Create (pTest2, WND_TYPE_STATIC, WND_STYLE_NORMAL, WND_STATUS_INVALID, 60, 50, 110, 50, 1);
	pTest3->SetText ("Static控件",20);
	pTest3->m_wStyle |= WND_STYLE_ROUND_EDGE;

	// OButton控件
	OButton* pTest4 = new OButton ();
	pTest4->Create (pTest2, WND_TYPE_BUTTON, WND_STYLE_NORMAL, WND_STATUS_NORMAL, 70, 70, 40, 20, 2);
	pTest4->SetText ("按钮",20);
	pTest4->m_wStyle |= WND_STYLE_ROUND_EDGE;
	pTest4->m_wStyle |= WND_STYLE_ORIDEFAULT;
//	pTest4->m_wStatus|= WND_STATUS_DEFAULT;
//	pTest4->m_wStatus|= WND_STATUS_FOCUSED;
//	pTest4->m_wStatus|= WND_STATUS_PUSHED;

	// 添加第二个按钮
	OButton* pTest5 = new OButton ();
	pTest5->Create (pTest2, WND_TYPE_BUTTON, WND_STYLE_NORMAL, WND_STATUS_NORMAL, 120, 70, 40, 20, 3);
	pTest5->SetText ("按钮",20);
	pTest5->m_wStyle |= WND_STYLE_ROUND_EDGE;

	// 定时器试验
//	g_Desktop.m_pMainWnd->m_pDesktop->SetTimer (g_Desktop.m_pMainWnd, 100, 5);
//	pTest->m_pDesktop->SetTimer (pTest, 200, 6);
//	pTest2->m_pDesktop->SetTimer (pTest2, 300, 7);
	// 定时器试验结束
*/

/*
	// 脱字符试验
	CARET caret;
	caret.bValid = TRUE;			// 是否使用脱字符
    caret.x = 40;					// 位置
    caret.y = 50;
    caret.w = 20;					// 宽高
	caret.h = 12;
    caret.bFlash = TRUE;			// 是否闪烁
    caret.bShow  = TRUE;			// (第一次出现应该处于显示状态)
    caret.lTimeInterval = 1;		// 闪烁的时间间隔(一般采用500毫秒)
	g_Desktop.m_pCaret->SetCaret (&caret);
	// 脱字符试验结束
*/
}

// Win32的消息处理函数
LRESULT CALLBACK WndProc (HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_PAINT:
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 1;
}
/* END */