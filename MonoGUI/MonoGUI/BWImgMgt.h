// BWImgMgt.h: interface for the BWImgMgt class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined (__BWIMGMGT_H__)
#define __BWIMGMGT_H__

typedef struct _BW_IMAGE_ITEM
{
	int id;
	BW_IMAGE* pImage;
} BW_IMAGE_ITEM;

class BWImgMgt
{
private:
	int m_nCount;
	int m_nReserveSize;
	BW_IMAGE_ITEM* m_pcaImages;
	
public:
	BWImgMgt();
	virtual ~BWImgMgt();
	void Clear ();
	BOOL Reserve (int nSize);
	BOOL Add (int nImgID, BW_IMAGE* pImg);
	BW_IMAGE* GetImage (int nImgID);
};

#endif // !defined(__BWIMGMGT_H__)
