// OButton.h: interface for the OButton class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__OBUTTON_H__)
#define __OBUTTON_H__


class OButton : public OWindow
{
private:
	enum { self_type = WND_TYPE_BUTTON };

public:
	OButton ();
	virtual ~OButton ();

	BOOL Create(OWindow* pParent,
		WORD wStyle,
		WORD wStatus,
		int x,
		int y,
		int w,
		int h,
		int ID
	);
	
	// 虚函数，绘制按钮
	virtual void Paint (LCD* pLCD);

	// 虚函数，消息处理
	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, int iMsg, int wParam, int lParam);

#if defined (MOUSE_SUPPORT)
	// 坐标设备消息处理
	virtual int PtProc (OWindow* pWnd, int iMsg, int wParam, int lParam);
#endif // defined(MOUSE_SUPPORT)

private:
	// 按键被按下的处理
	void OnPushDown ();
};

#endif // !defined(__OBUTTON_H__)
