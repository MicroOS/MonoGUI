// OApp.cpp: implementation of the OApp class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if defined (RUN_ENVIRONMENT_LINUX)
#include "init.h"
#endif // defined(RUN_ENVIRONMENT_LINUX)

#include "MonoGUI.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
OApp::OApp()
{
	m_bReverseShow = FALSE;

	m_pLCD        = new LCD();
	m_pScreenBuffer = new LCD(SCREEN_W, SCREEN_H);
	m_pMsgQ       = new OMsgQueue();
	m_pMainWnd    = NULL;
	m_pCaret      = new OCaret();
	m_pTimerQ     = new OTimerQueue();
	m_pTopMostWnd = NULL;
	m_pKeyMap     = new KeyMap();
	m_pClock      = new OClock(this);
	m_pSysBar     = new OSystemBar();
	m_pImageMgt   = new BWImgMgt();

#if defined (CHINESE_SUPPORT)

	m_pIMEWnd     = new OIME();
#endif // defined(CHINESE_SUPPORT)

	m_bClockKeyEnable = TRUE;
}

OApp::~OApp()
{
#if defined (RUN_ENVIRONMENT_LINUX)
	if (m_pLCD != NULL)
		delete m_pLCD;
#endif // defined(RUN_ENVIRONMENT_LINUX)

	if (m_pLCD) {
		delete m_pLCD;
	}
	if (m_pScreenBuffer != NULL) {
		delete m_pScreenBuffer;
	}
	if (m_pMsgQ != NULL) {
		delete m_pMsgQ;
	}
	m_pMainWnd = NULL;
	if (m_pCaret != NULL) {
		delete m_pCaret;
	}
	if (m_pTimerQ != NULL) {
		delete m_pTimerQ;
	}
	m_pTopMostWnd = NULL;
	if (m_pKeyMap != NULL) {
		delete m_pKeyMap;
	}
	if (m_pClock != NULL) {
		delete m_pClock;
	}
	if (m_pSysBar != NULL) {
		delete m_pSysBar;
	}
	if (m_pImageMgt != NULL) {
		delete m_pImageMgt;
	}

#if defined (CHINESE_SUPPORT)
	if (m_pIMEWnd != NULL) {
		delete m_pIMEWnd;
	}
#endif // defined(CHINESE_SUPPORT)

#if defined (RUN_ENVIRONMENT_WIN32)
	m_pLCD->Win32Fini ();
#endif //defined()

#if defined (RUN_ENVIRONMENT_LINUX)
	m_pLCD->LinuxFini ();
#endif //defined()
}

// 调用一次切换一次显示模式，并返回切换后的模式
BOOL OApp::ReverseShow(void)
{
	m_bReverseShow = (m_bReverseShow ? 0 : 1);
	return m_bReverseShow;
}

// 绘制窗口
void OApp::PaintWindows (OWindow* pWnd)
{
	// 调用主窗口的绘制函数
	pWnd->Paint (m_pScreenBuffer);

#if defined (CHINESE_SUPPORT)

	// 调用输入法窗口的绘制函数
	m_pIMEWnd->Paint (m_pScreenBuffer);
#endif // defined(CHINESE_SUPPORT)

	// 绘制TopMost窗口
	if (IsWindowValid (m_pTopMostWnd)) {
		m_pTopMostWnd->Paint(m_pScreenBuffer);
	}

	// 绘制系统状态条
	m_pSysBar->Show(m_pScreenBuffer);

#if defined (MOUSE_SUPPORT)
	// 绘制时钟按钮
	m_pClock->ShowClockButton(m_pScreenBuffer);
#endif // defined(MOUSE_SUPPORT)

	// 将绘制好的buf传给真正的屏幕
	m_pLCD->Copy (*m_pScreenBuffer, 0);

	// 绘制脱字符
	m_pCaret->DrawCaret (m_pLCD, m_pScreenBuffer);

	Show ();
}

#if defined (RUN_ENVIRONMENT_LINUX)
BOOL OApp::Create (CLinuxAdaptor* pLinuxAdaptor, COWindow* pMainWnd)
{

	if (pMainWnd == NULL) {
		return FALSE;
	}

	if (pDevice == NULL) {
		return FALSE;
	}

	m_pLinuxDevice = pLinuxAdaptor;

	m_pLCD->LinuxInit ();

	m_pMainWnd = pMainWnd;
	m_pMainWnd->m_pApp = this;

	// 创建按键宏与按键值对照表
	if (! m_pKeyMap->Load(KEY_MAP_FILE )) {
		return FALSE;
	}

	// 显示开始画面
	ShowStart ();

#if defined (CHINESE_SUPPORT)

	// 创建输入法窗口
	if (! m_pIMEWnd->Create(this)) {
		return FALSE;
	}
#endif // defined(CHINESE_SUPPORT)

	m_pMainWnd->UpdateView (m_pMainWnd);
	return TRUE;
}
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
BOOL OApp::Create (OWindow* pMainWnd, HWND hWnd)
{
	if (pMainWnd == NULL || hWnd == NULL) {
		return FALSE;
	}

	m_hWnd = hWnd;

	m_pLCD = new LCD;
	m_pLCD->Win32Init (SCREEN_W, SCREEN_H);

	m_pMainWnd = pMainWnd;
	m_pMainWnd->m_pApp = this;

	// 创建按键宏与按键值对照表
	if (! m_pKeyMap->Load(KEY_MAP_FILE)) {
		return FALSE; 
	}

	// 显示开始画面
	ShowStart ();

#if defined (CHINESE_SUPPORT)
	// 创建输入法窗口
	if (! m_pIMEWnd->Create(this)) {
		return FALSE;
	}
#endif // defined(CHINESE_SUPPORT)

	m_pMainWnd->UpdateView (m_pMainWnd);
	return TRUE;
}
#endif // defined(RUN_ENVIRONMENT_WIN32)

BOOL OApp::Run ()
{
	int nMsgInfo = 1;
	O_MSG msg;

	while (nMsgInfo != 0)
	{
		int nMsgInfo = m_pMsgQ->GetMsg (&msg);

		if (nMsgInfo == 1) {
			DespatchMsg (&msg);
		}
		else if (nMsgInfo == -1) {
			Idle();
		}
	}

	// run循环结束，退出程序
	return TRUE;
}

#if defined (RUN_ENVIRONMENT_WIN32)
// 专用于Win32仿真环境

void OApp::RunInWin32 ()
{
	int nMsgInfo = 1;
	O_MSG msg;

	while (nMsgInfo != -1)
	{
		nMsgInfo = m_pMsgQ->GetMsg (&msg);
		if (nMsgInfo == 1)
		{
			DespatchMsg (&msg);
		}
		else if (nMsgInfo == 0)
		{
			// 退出模拟器程序
			::PostQuitMessage (WM_QUIT);
		}
	}

	Idle();
}
#endif // defined(RUN_ENVIRONMENT_WIN32)

// 消息队列空状态下的处理
void OApp::Idle()
{
	GetKeyboardEvent();				// 将按键事件转换为消息

	m_pTimerQ->CheckTimer(this);	// 检查定时器队列，有到时的则发送定时器消息

	// 如果CLOCK窗口处于打开状态，则不更新脱字符
	if (m_pClock->IsEnable()) {
		if (m_pClock->Check (m_pLCD, m_pScreenBuffer)) {
			Show ();
		}
	}
	else {
		m_pCaret->Check (m_pLCD, m_pScreenBuffer);	// 更新脱字符
	}

	// 空闲处理
	OnIdle();
}

// 显示开机画面
void OApp::ShowStart()
{
}

// 空闲处理
void OApp::OnIdle()
{
#if defined (RUN_ENVIRONMENT_WIN32)
	Sleep (10); //避免界面刷新过于频繁
#endif // defined(RUN_ENVIRONMENT_WIN32)
}

// 蜂鸣器鸣叫指示检测到按键按键消息
void OApp::Beep()
{
}

// 刷新显示
void OApp::Show()
{
#if defined (RUN_ENVIRONMENT_LINUX)
	m_pLCD->Show ();
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
	m_pLCD->Show (m_hWnd, m_bReverseShow);
#endif // defined(RUN_ENVIRONMENT_WIN32)
}

// 获取键盘事件，插入消息队列；
// 直接将键盘键码作为参数插入消息队列；
// 如果该键值是按键按下的值(值小于128)
// 则调用IAL层的getch函数实现。iMsg = OM_KEYDOWN；WPARAM = 键值
#if defined (RUN_ENVIRONMENT_WIN32)
BOOL OApp::GetKeyboardEvent ()
{
	return FALSE;
}
#endif // defined(RUN_ENVIRONMENT_WIN32)

#if defined (RUN_ENVIRONMENT_LINUX)
BOOL OApp::GetKeyboardEvent ()
{
	BYTE ch = 0;
	BYTE cKeyValue = 0;

	while (ch = getch())	// 取键值
	{
		if (ch < 128) {
			cKeyValue = ch;
		}
		if (ch == 0) {
			break;
		}
	}

	if (cKeyValue == 0) {
		return TRUE;
	}

	if (m_pClock->IsEnable())
	{
		m_pClock->Enable (FALSE);
	}
	else if (cKeyValue == KEY_CAPS_LOCK)
	{
		// 翻转Caps的状态
		if (m_pSysBar->GetCaps()) {
			m_pSysBar->SetCaps (FALSE);
		}
		else {
			m_pSysBar->SetCaps (TRUE);
		}

		// 重绘窗口
		PaintWindows (m_pMainWnd);
	}
	else
	{
		if (m_bClockKeyEnable)
		{
			if (cKeyValue == KEY_CLOCK)
			{
				if (m_pClock->IsEnable()) {
					m_pClock->Enable (FALSE);
				}
				else {
					m_pClock->Enable (TRUE);
				}
			}
			else
			{
				PostKeyMsg (cKeyValue);
			}
		}
		else
		{
			PostKeyMsg (cKeyValue);
		}
	}

	Beep ();
	return TRUE;
}
#endif // defined(RUN_ENVIRONMENT_LINUX)

void OApp::PostKeyMsg (BYTE cKeyValue)
{
	// 获得特殊功能键的状态
	int nKeyMask = 0x00000000;
	if (m_pSysBar->GetCaps()) {
		nKeyMask |= CAPSLOCK_MASK;
	}
	if (GetKeyState(VK_SHIFT) & 0x80) {
		nKeyMask |= SHIFT_MASK;
	}

	O_MSG msg;
	msg.pWnd    = m_pMainWnd;
	msg.message = OM_KEYDOWN;
	msg.wParam  = cKeyValue;
	msg.lParam  = nKeyMask;
	m_pMsgQ->PostMsg (&msg);
}

// 清除按键缓冲区残留的内容，用于比较耗时的操作结束时
#if defined (RUN_ENVIRONMENT_WIN32)
BOOL OApp::CleanKeyBuffer()
{
	return TRUE;
}
#endif // defined(RUN_ENVIRONMENT_WIN32)

#if defined (RUN_ENVIRONMENT_LINUX)
BOOL OApp::CleanKeyBuffer()
{
	int i;
	for (i=0; i<1000; i++)
	{
		if (getch() == 0) {
			return TRUE;
		}
	}
	return FALSE;
}
#endif // defined(RUN_ENVIRONMENT_LINUX)

// 发送消息
// 区分出OM_PAINT消息单独处理
int OApp::DespatchMsg (O_MSG* pMsg)
{
	
#if defined (MOUSE_SUPPORT)

	// 区分出鼠标消息单独处理
	if ((pMsg->message == OM_LBUTTONDOWN) ||
		(pMsg->message == OM_LBUTTONUP)   ||
		(pMsg->message == OM_MOUSEMOVE))
	{
		if (pMsg->message == OM_LBUTTONDOWN)
		{
			// 首先处理点击时钟按钮的消息
			if (m_pClock->PtProc (pMsg->wParam, pMsg->lParam))
			{
				PaintWindows (m_pMainWnd);
				return 0;
			}

			// 鼠标点击SystemBar上大小写转换按钮的处理
			if (m_pSysBar->PtProc (pMsg->wParam, pMsg->lParam)) {
				return 0;
			}
		}

#if defined (CHINESE_SUPPORT)
		// 如果输入法窗口处于打开状态，则首先传递给输入法窗口处理
		if (IsIMEOpen())
		{
			if (m_pIMEWnd->PtInWindow (pMsg->wParam, pMsg->lParam))
			{
				m_pIMEWnd->PtProc (pMsg->pWnd, pMsg->message, pMsg->wParam, pMsg->lParam);
				return 0;
			}
		}
#endif // defined(CHINESE_SUPPORT)

		if (IsWindowValid (pMsg->pWnd))
		{
			 return pMsg->pWnd->PtProc (pMsg->pWnd, pMsg->message, pMsg->wParam, pMsg->lParam);
		}
	}

	// 区分出OM_PAINT消息单独处理
	if (pMsg->message == OM_PAINT)
	{
		PaintWindows (pMsg->pWnd);
		return 0;
	}

	return (SendMsg (pMsg));
#else	
	if (pMsg->message == OM_PAINT)
	{
		PaintWindows (pMsg->pWnd);
		return 0;
	}

	return (SendMsg (pMsg));
#endif // defined(MOUSE_SUPPORT)

}

#if defined (RUN_ENVIRONMENT_WIN32)
// 模仿GetKeyboardEvent函数向MonoGUI系统的消息队列中插入一个键盘消息
// 该函数仅用于windows下仿真,在Win32下替换掉DispatchMessage函数
void OApp::InsertMsgToMonoGUI (MSG* pMSG)
{
	// 只插入键盘消息、鼠标左键按下消息和WM_PAINT消息
	if (pMSG->message == WM_KEYDOWN)
	{
		if (m_pClock->IsEnable())
		{
			m_pClock->Enable(FALSE);
		}
		else if (pMSG->wParam == KEY_CAPS_LOCK)
		{
			// 翻转Caps的状态
			if (m_pSysBar->GetCaps()) {
				m_pSysBar->SetCaps(FALSE);
			}
			else {
				m_pSysBar->SetCaps(TRUE);
			}

			// 重绘窗口
			PaintWindows (m_pMainWnd);
		}
		else
		{
			if (m_bClockKeyEnable)
			{
				if (pMSG->wParam == KEY_CLOCK)
				{
					if (m_pClock->IsEnable()) {
						m_pClock->Enable(FALSE);
					}
					else {
						m_pClock->Enable(TRUE);
						// 重绘窗口，擦掉时钟按钮
						PaintWindows (m_pMainWnd);
					}
				}
				else
				{
					PostKeyMsg (pMSG->wParam);
				}
			}
			else
			{
				PostKeyMsg (pMSG->wParam);
			}
		}

		Beep();
	}

#if defined (MOUSE_SUPPORT)
	// 只响应鼠标左键
	if ((pMSG->message == WM_LBUTTONDOWN) ||
		(pMSG->message == WM_LBUTTONUP)   ||
		(pMSG->message == WM_MOUSEMOVE))
	{
		// 鼠标点击则关闭Clock显示
		if (m_pClock->IsEnable())
		{
			if (pMSG->message == WM_LBUTTONDOWN) {
				m_pClock->Enable (FALSE);
			}
			return;
		}

		int x = (WORD)(pMSG->lParam);
		int y = (WORD)(((DWORD)(pMSG->lParam) >> 16) & 0xFFFF);

		// 根据屏幕的缩放状况进行坐标变换
		x = x / SCREEN_MODE;
		y = y / SCREEN_MODE;

		O_MSG msg;
		msg.pWnd	= m_pMainWnd;
		msg.wParam  = x;
		msg.lParam  = y;

		switch (pMSG->message)
		{
		case WM_LBUTTONDOWN:
			msg.message = OM_LBUTTONDOWN;
			break;
		case WM_LBUTTONUP:
			msg.message = OM_LBUTTONUP;
			break;
		case WM_MOUSEMOVE:
			msg.message = OM_MOUSEMOVE;
			break;
		}

		m_pMsgQ->PostMsg (&msg);
	}
#endif // defined(MOUSE_SUPPORT)

	// 如果CLOCK窗口处于打开状态，
	// 除MonoGUI系统状态条所需消息之外的所有消息都会被拦截
	if (m_pClock->IsEnable()) {
		return;
	}

	// Clock关闭状态下允许系统WM_PAINT消息
	if (pMSG->message == WM_PAINT)
	{
		O_MSG msg;
		msg.pWnd	= m_pMainWnd;
		msg.message = OM_PAINT;
		msg.wParam  = 0;
		msg.lParam  = 0;
		m_pMsgQ->PostMsg (&msg);
	}
}
#endif // defined(RUN_ENVIRONMENT_WIN32)

// 下面的函数调用成员类的相应函数实现

#if defined (CHINESE_SUPPORT)
// 打开输入法窗口(打开显示，创建联系)
BOOL OApp::OpenIME (OWindow* pWnd)
{
	return (m_pIMEWnd->OpenIME(pWnd));
}
#endif // defined(CHINESE_SUPPORT)

#if defined (CHINESE_SUPPORT)
// 关闭输入法窗口(关闭显示，断开联系)
BOOL OApp::CloseIME (OWindow* pWnd)
{
	return (m_pIMEWnd->CloseIME(pWnd));
}
#endif // defined(CHINESE_SUPPORT)

// 显示系统信息
BOOL OApp::ShowHint (int nIcon, char* psInfo)
{
	// 计算显示文字可能会占用的空间
	int nWidth = 0;
	int nHeight = 0;
	BYTE sTemp [WINDOW_CAPTION_BUFFER_LEN];

	// 最多不能超过四行文本
	int i;
	for (i = 0; i < 4; i++)
	{
		memset (sTemp, 0x0, sizeof(sTemp));
		if (GetLine (psInfo, (char *)sTemp, i))
		{
			int nDesplayLength = GetDisplayLength ((char *)sTemp,
				sizeof(sTemp)-1);
			if (nDesplayLength > nWidth) {
				nWidth = nDesplayLength;
			}

			nHeight += 14;
		}
	}

	if (nWidth > (SCREEN_W - 40)) {
		nWidth = SCREEN_W - 40;
	}

	int nTextLeft = 10;
	int tw = nWidth + 24;
	int th = nHeight + 42;

	// 留出图标的位置
	if (nIcon > 0) {
		tw += 30;
		nTextLeft += 30;
	}
	if (tw < 80) {
		tw = 80;
	}

	int w;
	int h;
	BYTE* fb = m_pLCD->GetBuffer(&w, &h);
	int tx = (w - tw) / 2;
	int ty = (h - th) / 2;

	// 清空背景区域并绘制边框
	m_pLCD->FillRect(tx, ty, tw, th, 0);
	m_pLCD->HLine   (tx, ty, tw, 1);
	m_pLCD->HLine   (tx, ty+th-1, tw, 1);
	m_pLCD->VLine   (tx, ty, th, 1);
	m_pLCD->VLine   (tx+tw-1, ty, th, 1);

	// 绘制图标
	FOUR_COLOR_IMAGE* pIcon = NULL;
	switch (nIcon)
	{
	case OMB_ERROR:
		pIcon = &g_4color_Icon_Error;
		break;

	case OMB_EXCLAMATION:
		pIcon = &g_4color_Icon_Exclamation;
		break;

	case OMB_QUESTION:
		pIcon = &g_4color_Icon_Question;
		break;

	case OMB_INFORMATION:
		pIcon = &g_4color_Icon_Information;
		break;

	case OMB_BUSY:
		pIcon = &g_4color_Icon_Busy;
		break;

	case OMB_PRINT:
		pIcon = &g_4color_Icon_Printer;
		break;
	}
	m_pLCD->DrawImage (tx+10,ty+18,23,23,*pIcon,0,0, LCD_MODE_NORMAL);

	// 拆分并绘制文本
	// 最多不能超过四行文本
	for (i = 0; i < 4; i++)
	{
		memset (sTemp, 0x0, sizeof(sTemp));
		if (GetLine (psInfo, (char *)sTemp, i)) {
			m_pLCD->TextOut (tx+nTextLeft,ty+21+i*14,sTemp,sizeof(sTemp)-1, LCD_MODE_NORMAL);
		}
	}

	Show ();
	return TRUE;
}

// 关闭系统提示
BOOL OApp::CloseHint()
{
	m_pLCD->Copy (*m_pScreenBuffer, 0);
	CleanKeyBuffer();
	return TRUE;
}

// 直接调用消息处理函数；
int OApp::SendMsg (O_MSG* pMsg)
{
	if (IsWindowValid (pMsg->pWnd))
	{
		return pMsg->pWnd->Proc (pMsg->pWnd, pMsg->message, pMsg->wParam, pMsg->lParam);
	}
	else
	{
		printf("SendMsg: Invalid Window !\n");
	}

	return 0;
}

// 向消息队列中添加一条消息；
// 如果消息队列满（消息数量达到了MESSAGE_MAX 所定义的数目），则返回失败；
BOOL OApp::PostMsg (O_MSG* pMsg)
{
	return m_pMsgQ->PostMsg (pMsg);
}

// 向消息队列中添加一条退出消息；
BOOL OApp::PostQuitMsg()
{
	O_MSG msg;
	msg.pWnd = NULL;
	msg.message = OM_QUIT;
	msg.wParam = 0;
	msg.lParam = 0;
	return m_pMsgQ->PostMsg (&msg);
}

// 在消息队列中查找指定类型的消息；
// 如果发现消息队列中有指定类型的消息，则返回TRUE；
// 该函数主要用在定时器处理上。CheckTimer函数首先检查消息队列中有没有相同的定时器消息，如果没有，再插入新的定时器消息
BOOL OApp::FindMsg (O_MSG* pMsg)
{
	return m_pMsgQ->FindMsg (pMsg);
}

// 根据窗口的脱字符信息设置系统脱字符的参数；
BOOL OApp::SetCaret (CARET* pCaret)
{
	return m_pCaret->SetCaret (pCaret);
}

// 添加一个定时器；
// 如果当前定时器的数量已经达到TIMER_MAX所定义的数目，则返回FALSE；
// 如果发现一个ID与当前定时器相同的定时器，则直接修改该定时器的设定；
BOOL OApp::SetTimer (OWindow* pWindow, int nTimerID, int interval)
{
	return m_pTimerQ->SetTimer (pWindow, nTimerID, interval);
}

// 删除一个定时器；
// 根据TimerID删除
BOOL OApp::KillTimer (int nTimerID)
{
	return m_pTimerQ->KillTimer (nTimerID);
}

#if defined (CHINESE_SUPPORT)
// 看输入法窗口是否处于打开状态
BOOL OApp::IsIMEOpen ()
{
	return m_pIMEWnd->IsIMEOpen ();
}
#endif // defined(CHINESE_SUPPORT)

// 设置TopMost窗口
BOOL OApp::SetTopMost (OWindow* pWindow)
{
	if (IsWindowValid(pWindow))
	{
		m_pTopMostWnd = pWindow;
		return TRUE;
	}
	else
	{
		m_pTopMostWnd = NULL;
		return FALSE;
	}
}

// 检验一个窗口指针是否有效
BOOL OApp::IsWindowValid (OWindow* pWindow)
{
	if (pWindow != NULL)
	{
		WORD wType = pWindow->m_wWndType;
		if ((wType > WND_VALID_BEGIN) && (wType < WND_VALID_END))	// 判断TopMost窗口的有效性
		{
			return TRUE;
		}
	}
	return FALSE;
}

// 使能/禁止显示时钟
// 注：显示时钟使能时，按“当前时间”键打开时钟窗口。
// “当前时间”按键消息将不能发送给任何一个窗口。
// 机器自检时，需要禁止始终，以保证自检窗口可以接收到“当前时间”的按键消息
BOOL OApp::ClockKeyEnable (BOOL bEnable)
{
	// 如果禁止显示时钟，则首先关闭时钟窗口
	if (! bEnable)
	{
		if (m_pClock->IsEnable()) {
			m_pClock->Enable (FALSE);
		}
	}
	BOOL bTemp = m_bClockKeyEnable;
	m_bClockKeyEnable = bEnable;
	return bTemp;
}

// 设置时钟的显示位置
BOOL OApp::SetClockPos (int x, int y)
{
	return m_pClock->SetClockPos (x, y);
}

#if defined (MOUSE_SUPPORT)
// 设置时钟按钮的位置
BOOL OApp::SetClockButton (int x, int y)
{
	return m_pClock->SetClockButton (x, y);
}
#endif // defined(MOUSE_SUPPORT)

/* END */