// OStatic.h: interface for the OStatic class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__OSTATIC_H__)
#define __OSTATIC_H__

class OStatic : public OWindow
{
private:
	enum { self_type = WND_TYPE_STATIC };

public:
	OStatic ();
	virtual ~OStatic ();

	// 定义静态图像所需要的图片
	BW_IMAGE* m_pImage;

public:
	// 创建编辑框
	BOOL Create
	(
		OWindow* pParent,
		WORD wStyle,
		WORD wStatus,
		int x,
		int y,
		int w,
		int h,
		int ID
	);

	// 调入图片
	BOOL SetImage (BW_IMAGE* pImage);

	void Paint (LCD* pLCD);
	int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);
};

#endif // !defined(__OSTATIC_H__)
